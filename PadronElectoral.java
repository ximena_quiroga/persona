
/**
 * Representa la clase PadronElectoral.
 * 
 * @author
 */
public class PadronElectoral
{
    private int numeroDeRegistroCivil;
    
    private String direccion;

    /**
     * Constructor for objects of class PadronElectoral
     */
    public PadronElectoral(int numeroDeRegistroCivil, String direccion)
    {
        this.numeroDeRegistroCivil = numeroDeRegistroCivil;
        this.direccion = direccion;
    }

    /**
     * Verifica si un ciudadano esta habilitado para votar de acuerdo a su edad.
     * 
     * @param  y   a sample parameter for a method
     * @return     the sum of x and y 
     */
    public boolean ciudadanoHabilitado(Persona persona)
    {
        if(persona.mostrarEdad() >= 18) {
            return true;
        }
        
        return false;
    }
}
