

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * The test class PadronElectoralTest.
 *
 * @author
 */
public class PadronElectoralTest
{
    @Test
    public void testCiudadanoHabilitado()
    {
        Persona persona = new Persona("704444", "Clara", "Almendras", 20, "Soltera");
        PadronElectoral padron = new PadronElectoral(1515, "Sacaba #123");
        boolean estadoCiudadano = padron.ciudadanoHabilitado(persona);
        
        assertTrue(estadoCiudadano);
    }
    
    @Test
    public void testCiudadanoNoHabilitado()
    {
        Persona persona = new Persona("8040111", "Raul", "Jordan", 15, "Soltero");
        PadronElectoral padron = new PadronElectoral(1515, "Sacaba #123");
        boolean estadoCiudadano = padron.ciudadanoHabilitado(persona);
        
        assertFalse(estadoCiudadano);
    }
}
