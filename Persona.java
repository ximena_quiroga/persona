
/**
 * Representa a una persona.
 * 
 * @author
 */
public class Persona
{
    // identificador de la persona
    private String identificador;
    
    // nombre de la persona
    private String nombre;
    
    // apellido de la persona
    private String apellido;
    
    // edad de la persona
    private int edad;
    
    // estado civil de la persona
    private String estadoCivil;

    /**
     * Constructor for objects of class Persona
     */
    public Persona(String identificador, String nombre, String apellido, int edad)
    {
        this.identificador = identificador;
        this.nombre = nombre;
        this.apellido = apellido;
        this.edad = edad;
    }
    
     /**
     * Constructor for objects of class Persona
     */
    public Persona(String identificador, String nombre, String apellido, int edad, String estadoCivil)
    {
        this.identificador = identificador;
        this.nombre = nombre;
        this.apellido = apellido;
        this.edad = edad;
        this.estadoCivil = estadoCivil;
    }

    /**
     * Incrementamos la edad de la persona en una unidad
     * 
     */
    public void incrementarEdad()
    {
       edad += 1;
    }
    
    /**
     * Cambia el estado civil de la persona, con el parametro provisto.
     * 
     * @param   nuevoEstadoCivil el nuevo estado que adoptara la persona.
     */
    public void cambiarEstadoCivil(String nuevoEstadoCivil) {
       estadoCivil = nuevoEstadoCivil;
    }
    
    /** Muestra la Informacion de la Persona
     * 
     * @return    una cadena con la informacion de la persona
     */
    public String mostrar() {
        return nombre + " " + apellido + " " + edad; 
    }
    
    /** Muestra la Informacion de la Persona
     * 
     * @return    una cadena con la informacion de la persona
     */
    public String mostrarEstadoCivil() {
        return nombre + " " + apellido + " " + edad + ", " + estadoCivil; 
    }
    
    /** Muestra la Edad de la persona
     * 
     * @return    la edad de la persona
     */
    public int mostrarEdad() {
        return edad; 
    }
}
