

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * The test class PersonaTest.
 *
 * @author  (your name)
 * @version (a version number or a date)
 */
public class PersonaTest
{
    @Test
    public void testIncrementarEdadPersona() {
        Persona persona = new Persona("658423", "Luis", "Flores", 30);
        persona.incrementarEdad();
        String informacion = persona.mostrar();
        assertEquals("Luis Flores 31", informacion);
    }
    
    @Test
    public void testCambiarEstadoCivil() {
        Persona persona = new Persona("658423", "Luis", "Flores", 30, "Soltero");
        persona.cambiarEstadoCivil("Casado");
        String informacion = persona.mostrarEstadoCivil();
        assertEquals("Luis Flores 30, Casado", informacion);
    }
}
